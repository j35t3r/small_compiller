%option noyywrap
%{
    #include "grammar.tab.h"
%}

alpha       [a-zA-Z]
name        {alpha}([0-9]*{alpha}*)+
whitespace  [ \r\t\f]
linefeed    \n

%%
{name}          { printf("Lex:name\n"); return NAME; }
":"             { printf("Lex:colon\n"); return COLON; }
"->"            { printf("Lex:r_arr\n"); return RIGHT_ARROW; }
"{"             { printf("Lex:l_arr\n"); return LEFT_BRACE; }
"}"             { printf("Lex:r_bra\n"); return RIGHT_BRACE; }
{whitespace}
{linefeed}      ++yylineno;
%%
