#include <iostream>

extern int yyparse();
extern int yylineno;

extern "C"
{
//  int yyparse();
  void yyerror(const char*);
  int yylex();
//  int yylineno;
}


int main()
{
  std::cout << "Hello, World!" << std::endl;

  int result = yyparse();
  if (result == 0)
    std::cout << "The input valid." << std::endl;
  else
    std::cout << "The input is invalid." << std::endl;


  std::cout << "The input of lines in the input is: " << yylineno << std::endl;


  return result;
}