all:
	$(MAKE) grammar
	$(MAKE) lex
	g++ compiler/grammar.tab.c compiler/lex.yy.c compiler/main.cpp

grammar:
	bison -d compiler/grammar.y
	mv grammar.tab.* compiler


lex:
	flex compiler/lex.l
	mv lex.yy.* compiler